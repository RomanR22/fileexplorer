﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileExplorer.Models
{
    class LogicalDrive : PropertyChangedBase
    {
        private DriveInfo logicalDriveInfo;
        public DriveInfo LogicalDriveInfo
        {
            get { return logicalDriveInfo; }
            set { logicalDriveInfo = value; NotifyOfPropertyChange(() => LogicalDriveInfo); }
        }

        private BindableCollection<PropertyChangedBase> children;
        public BindableCollection<PropertyChangedBase> Children
        {
            get { return children; }
            set { children = value; NotifyOfPropertyChange(() => Children); }
        }

        private BindableCollection<Folder> folders;
        public BindableCollection<Folder> Folders
        {
            get { return folders; }
            set { folders = value; NotifyOfPropertyChange(() => Folders); }
        }

        private BindableCollection<Models.File> files;
        public BindableCollection<Models.File> Files
        {
            get { return files; }
            set { files = value; NotifyOfPropertyChange(() => Files); }
        }

    }
}
