﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Caliburn.Micro;
using System.Windows.Media;

namespace FileExplorer.Models
{
    class Folder : PropertyChangedBase
    {
        private Folder selfFolder;
        public Folder SelfFolder
        {
            get { return selfFolder; }
            set { selfFolder = value; NotifyOfPropertyChange(() => SelfFolder); }
        }

        private ImageSource imageSource;
        public ImageSource ImageSource
        {
            get { return imageSource; }
            set { imageSource = value; NotifyOfPropertyChange(() => ImageSource); }
        }

        private DirectoryInfo directoryInfo;
        public DirectoryInfo DirectoryInfo
        {
            get { return directoryInfo; }
            set { directoryInfo = value; NotifyOfPropertyChange(() => DirectoryInfo); }
        }

        private BindableCollection<PropertyChangedBase> children;
        public BindableCollection<PropertyChangedBase> Children
        {
            get { return children; }
            set { children = value; NotifyOfPropertyChange(() => Children); }
        }

        private BindableCollection<Folder> folders;
        public BindableCollection<Folder> Folders
        {
            get { return folders; }
            set { folders = value; NotifyOfPropertyChange(() => Folders); }
        }

        private BindableCollection<Models.File> files;
        public BindableCollection<Models.File> Files
        {
            get { return files; }
            set { files = value; NotifyOfPropertyChange(() => Files); }
        }

    }
}
