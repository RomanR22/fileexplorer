﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace FileExplorer.Models
{
    class File : PropertyChangedBase
    {
        private FileInfo fileInfo;
        public FileInfo FileInfo
        {
            get { return fileInfo; }
            set { fileInfo = value; NotifyOfPropertyChange(() => FileInfo); }
        }

        private ImageSource imageSource;
        public ImageSource ImageSource
        {
            get { return imageSource; }
            set { imageSource = value; NotifyOfPropertyChange(() => ImageSource); }
        }
    }
}
