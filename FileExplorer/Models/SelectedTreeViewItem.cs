﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace FileExplorer.Models
{
    class SelectedTreeViewItem : PropertyChangedBase
    {
        private TreeViewItem treeViewItem;
        public TreeViewItem TreeViewItem
        {
            get { return treeViewItem; }
            set { treeViewItem = value; NotifyOfPropertyChange(() => TreeViewItem); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; NotifyOfPropertyChange(() => Name); }
        }

        private long size;
        public long Size
        {
            get { return size; }
            set { size = value; NotifyOfPropertyChange(() => Size); }
        }

        private string fullPath;
        public string FullPath
        {
            get { return fullPath; }
            set { fullPath = value; NotifyOfPropertyChange(() => FullPath); }
        }

        private bool isFolder;
        public bool IsFolder
        {
            get { return isFolder; }
            set { isFolder = value; NotifyOfPropertyChange(() => IsFolder); }
        }

        private int childFiles;
        public int ChildFiles
        {
            get { return childFiles; }
            set { childFiles = value; NotifyOfPropertyChange(() => ChildFiles); }
        }

        private int childFolders;
        public int ChildFolders
        {
            get { return childFolders; }
            set { childFolders = value; NotifyOfPropertyChange(() => ChildFolders); }
        }

        private string extension;
        public string Extension
        {
            get { return extension; }
            set { extension = value; NotifyOfPropertyChange(() => Extension); }
        }

        private BitmapImage preview;
        public BitmapImage Preview
        {
            get { return preview; }
            set { preview = value; NotifyOfPropertyChange(() => Preview); }
        }

    }
}
