﻿using FileExplorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FileExplorer.Views
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();
        }

        private void OnTreeItemRightMouseButtonClick(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);

            if (treeViewItem != null)
            {
                treeViewItem.Focus();
                e.Handled = true;
            }
        }

        static TreeViewItem VisualUpwardSearch(DependencyObject source)
        {
            while (source != null && !(source is TreeViewItem))
                source = VisualTreeHelper.GetParent(source);

            return source as TreeViewItem;
        }

        private void OnTreeExpanded(object sender, RoutedEventArgs e)
        {
            var treeViewItem = (TreeViewItem)sender;

            Models.Folder folder = (Models.Folder)treeViewItem.Header;

            MainWindowViewModel viewModel = DataContext as MainWindowViewModel;
            viewModel.SetChildren(folder);

            e.Handled = true;
        }

        private void OnTreeItemSelected(object sender, RoutedEventArgs e)
        {
            var treeViewItem = (TreeViewItem)sender;

            MainWindowViewModel viewModel = DataContext as MainWindowViewModel;
            viewModel.SetTreeViewItem(treeViewItem);

            e.Handled = true;
        }

        private void OnOpenClick(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;

            MainWindowViewModel viewModel = DataContext as MainWindowViewModel;
            viewModel.Open(viewModel.SelectedTreeVievItem.FullPath);

            e.Handled = true;
        }

        private void OnFolderExpandClick(object sender, RoutedEventArgs e)
        {
            var menuItem = (MenuItem)sender;

            MainWindowViewModel viewModel = DataContext as MainWindowViewModel;
            viewModel.SetChildren((Models.Folder)menuItem.Tag);
            viewModel.SelectedTreeVievItem.TreeViewItem.IsExpanded = true;

            e.Handled = true;
        }
    }
}
