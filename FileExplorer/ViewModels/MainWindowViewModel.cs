﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using FileExplorer.Models;
using System.IO;
using Caliburn.Micro;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Diagnostics;

namespace FileExplorer.ViewModels
{
    class MainWindowViewModel : Conductor<IScreen>
    {
        private BindableCollection<LogicalDrive> logicalDrives;
        public BindableCollection<LogicalDrive> LogicalDrives
        {
            get { return logicalDrives; }
            set { logicalDrives = value; NotifyOfPropertyChange(() => LogicalDrives); }
        }

        private SelectedTreeViewItem selectedTreeVievItem;
        public SelectedTreeViewItem SelectedTreeVievItem
        {
            get { return selectedTreeVievItem; }
            set { selectedTreeVievItem = value; NotifyOfPropertyChange(() => SelectedTreeVievItem); }
        }

        private BindableCollection<PropertyChangedBase> children;
        public BindableCollection<PropertyChangedBase> Children
        {
            get { return children; }
            set { children = value; NotifyOfPropertyChange(() => Children); }
        }

        private BindableCollection<Folder> childFolders;
        public BindableCollection<Folder> ChildFolders
        {
            get { return childFolders; }
            set { childFolders = value; NotifyOfPropertyChange(() => ChildFolders); }
        }

        private BindableCollection<Models.File> childFiles;
        public BindableCollection<Models.File> ChildFiles
        {
            get { return childFiles; }
            set { childFiles = value; NotifyOfPropertyChange(() => ChildFiles); }
        }

        public void SetAllLogicalDrives()
        {
            if (LogicalDrives == null)
                LogicalDrives = new BindableCollection<LogicalDrive>(GetAllLogicalDrives());
        }

        public void SetTreeViewItem(TreeViewItem treeViewItem)
        {
            if (SelectedTreeVievItem == null)
                SelectedTreeVievItem = new SelectedTreeViewItem();

            SelectedTreeVievItem.TreeViewItem = treeViewItem;
        }

        public void SelectTreeViewItem(object treeVievItem)
        {
            TreeViewItem tviTemp = SelectedTreeVievItem.TreeViewItem;
            SelectedTreeVievItem = new SelectedTreeViewItem();
            SelectedTreeVievItem.TreeViewItem = tviTemp;

            try
            {
                if (treeVievItem is Folder)
                {
                    FillSelecterTreeViewItemFields((Folder)treeVievItem);
                    ShowFolderInfo();
                }
                else if (treeVievItem is Models.File)
                {
                    FillSelecterTreeViewItemFields((Models.File)treeVievItem);
                    ShowFileInfo();
                }
            }
            catch (Exception ex) { Console.WriteLine("Exeption SelectTreeViewItem: " + ex.Message); }

        }

        public void SetChildren(LogicalDrive logicalDrive)
        {
            string path = GetPath(logicalDrive);
            Children = new BindableCollection<PropertyChangedBase>(GetChildFoldersAndFiles(path));
        }

        public void SetChildren(Folder folder)
        {
            string path = GetPath(folder);
            folder.Children = new BindableCollection<PropertyChangedBase>(GetChildFoldersAndFiles(path));
        }

        public void Open(string path)
        {
            Process.Start(path);
        }

        private BindableCollection<LogicalDrive> GetAllLogicalDrives()
        {
            BindableCollection<LogicalDrive> logicalDrives = new BindableCollection<LogicalDrive>();

            try
            {
                DriveInfo[] drivesInfo = DriveInfo.GetDrives();

                foreach (DriveInfo driveInfo in drivesInfo)
                {
                    LogicalDrive logicalDrive = new LogicalDrive() { LogicalDriveInfo = driveInfo };
                    logicalDrives.Add(logicalDrive);
                }
            }
            catch (Exception ex) { Console.WriteLine("Exeption GetAllLogicalDrives: " + ex.Message); }

            return logicalDrives;
        }

        private BindableCollection<Folder> GetChildFolders(string path)
        {
            BindableCollection<Folder> folders = new BindableCollection<Folder>();

            try
            {
                DirectoryInfo[] foldersInfo = new DirectoryInfo(path).GetDirectories();

                foreach (DirectoryInfo directoryInfo in foldersInfo)
                {
                    Folder folder = new Folder();
                    folder.DirectoryInfo = directoryInfo;
                    folder.SelfFolder = folder;
                    folder.ImageSource = FolderManager.GetImageSource(folder.DirectoryInfo.FullName, ItemState.Close);
                    folders.Add(folder);
                }
            }
            catch (Exception ex) { Console.WriteLine("Exeption GetChildFolders: " + ex.Message); }

            return folders;
        }

        private BindableCollection<Models.File> GetChildFiles(string path)
        {
            BindableCollection<Models.File> files = new BindableCollection<Models.File>();

            try
            {
                FileInfo[] filesInfo = new DirectoryInfo(path).GetFiles();

                foreach (FileInfo fileInfo in filesInfo)
                {
                    Models.File file = new Models.File();
                    file.FileInfo = fileInfo;
                    file.ImageSource = FileManager.GetImageSource(file.FileInfo.FullName);
                    files.Add(file);
                }
            }
            catch (Exception ex) { Console.WriteLine("Exeption GetChildFiles: " + ex.Message); }

            return files;
        }

        private BindableCollection<PropertyChangedBase> GetChildFoldersAndFiles(string path)
        {
            BindableCollection<PropertyChangedBase> Children = new BindableCollection<PropertyChangedBase>();

            childFolders = GetChildFolders(path);
            childFiles = GetChildFiles(path);

            foreach (Folder folder in childFolders)
            {
                folder.Folders = GetChildFolders(folder.DirectoryInfo.FullName);
                folder.Files = GetChildFiles(folder.DirectoryInfo.FullName);

                folder.Children = new BindableCollection<PropertyChangedBase>();

                foreach (Folder folderChildren in folder.Folders)
                    folder.Children.Add(folderChildren);
                foreach (Models.File fileChildren in folder.Files)
                    folder.Children.Add(fileChildren);

                Children.Add(folder);
            }

            foreach (Models.File file in childFiles)
                Children.Add(file);

            return Children;
        }

        private void FillSelecterTreeViewItemFields(Folder folder)
        {
            DirectoryInfo directoryInfo = folder.DirectoryInfo;

            SelectedTreeVievItem.Name = directoryInfo.Name;
            SelectedTreeVievItem.FullPath = directoryInfo.FullName;
            SelectedTreeVievItem.IsFolder = true;
            SelectedTreeVievItem.ChildFiles = directoryInfo.GetFiles().Length;
            SelectedTreeVievItem.ChildFolders = directoryInfo.GetDirectories().Length;
        }

        private void FillSelecterTreeViewItemFields(Models.File file)
        {
            FileInfo fileInfo = file.FileInfo;

            SelectedTreeVievItem.Name = fileInfo.Name;
            SelectedTreeVievItem.Size = fileInfo.Length;
            SelectedTreeVievItem.FullPath = fileInfo.FullName;
            SelectedTreeVievItem.IsFolder = false;
            SelectedTreeVievItem.Extension = fileInfo.Extension;

            switch(fileInfo.Extension.ToLower())
            {
                case ".png":
                case ".jpeg":
                case ".jpg":
                case ".bmp":
                    SelectedTreeVievItem.Preview = new BitmapImage(new Uri(fileInfo.FullName));
                    break;
            }
        }

        private void ShowFolderInfo()
        {
            ActivateItem(new FolderViewModel() { SelectedTreeVievItem = SelectedTreeVievItem });
        }

        private void ShowFileInfo()
        {
            ActivateItem(new FileViewModel() { SelectedTreeVievItem = SelectedTreeVievItem });
        }

        private string GetPath(LogicalDrive logicalDrive)
        {
            return logicalDrive.LogicalDriveInfo.RootDirectory.FullName;
        }

        private string GetPath(Folder folder)
        {
            return folder.DirectoryInfo.FullName;
        }

    }
}
