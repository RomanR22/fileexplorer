﻿using Caliburn.Micro;
using FileExplorer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileExplorer.ViewModels
{
    class FileViewModel : Screen
    {
        protected override void OnActivate()
        {
            base.OnActivate();
        }

        private SelectedTreeViewItem selectedTreeVievItem;
        public SelectedTreeViewItem SelectedTreeVievItem
        {
            get { return selectedTreeVievItem; }
            set { selectedTreeVievItem = value; NotifyOfPropertyChange(() => SelectedTreeVievItem); }
        }
    }
}
